import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { MdKeyboardArrowRight } from 'react-icons/md';
import { FiPlus } from 'react-icons/fi';

import Card from '../components/list-car/Card';
import Loader from '../components/Loader';

export const sizeFilterGenerator = () => {
  const sizes = ['All', 'Small', 'Medium', 'Large'];
  const components = [];

  for (let size of sizes) {
    components.push(
      <div key={size} className='border-2 border-blue-700 px-2 py-1 rounded cursor-pointer hover:bg-blue-700'>
        <p className='text-blue-700 hover:text-white font-semibold'>{size}</p>
      </div>
    );
  }

  return components;
};

const ListCar = () => {
  const navigate = useNavigate();

  const [cars, setCars] = useState([]);
  const [loading, setLoading] = useState(false);

  const getCars = async () => {
    try {
      setLoading(true);
      let dataFromAPI = await fetch(`https://rent-cars-api.herokuapp.com/admin/car`);
      let dataToJSON = await dataFromAPI.json();
      setCars(await dataToJSON);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCars();
  }, []);

  const handleClick = (tab) => {
    navigate(`/dashboard?tab=${tab}`);
  };

  if (loading) return <Loader />;

  return (
    <div className='min-h-screen bg-slate-100 flex flex-col p-4'>
      <div className='flex flex-row font-bold items-center mt-4 mb-6'>
        <p>Cars</p>
        <MdKeyboardArrowRight size={20} />
        <p className='text-gray-600'>List Car</p>
      </div>
      <div className='flex flex-row justify-between'>
        <h2 className='text-2xl font-bold mb-3'>List Car</h2>
        <button className='bg-blue-700 text-white flex flex-row items-center rounded p-2 font-semibold' onClick={() => handleClick('add')}>
          <FiPlus />
          <p className='ml-1'>Add New Car</p>
        </button>
      </div>
      <div className='flex flex-row flex-wrap gap-2 mb-5'>{sizeFilterGenerator()}</div>
      <div className='flex flex-row flex-wrap gap-6'>
        {cars.map((car, index) => <Card key={index} id={car.id} name={car.name} category={car.category} finish={car.finish_rent_at} image={car.image} price={car.price} start={car.start_rent_at} status={car.status} updated={car.updatedAt} />)}
      </div>
    </div>
  );
};

export default ListCar;