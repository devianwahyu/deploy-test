import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { MdKeyboardArrowRight } from 'react-icons/md';


const AddCar = () => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState('');

  const navigate = useNavigate();

  const handleClickTab = (tab) => {
    navigate(`/dashboard?tab=${tab}`);
  };

  const handleSubmit = () => {
    let formData = new FormData();
    formData.append('name', name);
    formData.append('category', 'Medium');
    formData.append('price', price);
    formData.append('status', false);
    formData.append('image', image);

    axios.post('https://rent-cars-api.herokuapp.com/admin/car', formData)
      .then(response => console.log(response))
      .catch(error => console.log(error))
      .finally(() => navigate('/dashboard?tab=car'));
  };

  return (
    <div className='min-h-screen bg-slate-100 flex flex-col p-4 justify-between'>
      <div>
        <div className='flex flex-row font-bold items-center mt-4 mb-6'>
          <p>Cars</p>
          <MdKeyboardArrowRight size={20} />
          <p className='cursor-pointer' onClick={() => handleClickTab('car')}>List Car</p>
          <MdKeyboardArrowRight size={20} />
          <p className='text-gray-600 cursor-pointer' onClick={() => handleClickTab('add')}>Add New Car</p>
        </div>
        <h2 className='text-2xl font-bold mb-3'>Add New Car</h2>
        <div className='flex flex-row'>
          <div className='flex flex-col flex-grow gap-5 px-3 py-5 bg-white rounded'>
            <div className='flex flex-row items-center justify-between'>
              <label className='text-gray-600' htmlFor='name'>Nama*</label>
              <input className='border-2 w-[70%] border-gray-600 rounded p-2' type={'text'} id='name' onChange={e => setName(e.target.value)} />
            </div>
            <div className='flex flex-row items-center justify-between'>
              <label className='text-gray-600' htmlFor='harga'>Harga*</label>
              <input className='border-2 w-[70%] border-gray-600 rounded p-2' type={'text'} id='harga' onChange={e => setPrice(e.target.value)} />
            </div>
            <div className='flex flex-row items-center justify-between'>
              <label className='text-gray-600' htmlFor='image'>Foto*</label>
              <input className='border-2 w-[70%] border-gray-600 rounded p-2' type={'file'} id='image' onChange={e => setImage(e.target.files[0])} accept='image/*' />
            </div>
            <p className='text-gray-600'>Start Rent</p>
            <p className='text-gray-600 mt-3'>Finish Rent</p>
            <p className='text-gray-600 mt-3'>Created at</p>
            <p className='text-gray-600 mt-3'>Updated at</p>
          </div>
          <div className='flex w-[35vw] bg-white'></div>
        </div>
      </div>
      <div className='flex flex-row'>
        <button className='w-20 p-2 border-2 rounded font-semibold border-red-700 text-red-700 hover:bg-red-700 hover:text-white'>Cancel</button>
        <button className='w-20 ml-4 p-2 border-2 rounded font-semibold border-blue-700 bg-blue-700 text-white' onClick={handleSubmit}>Save</button>
      </div>
    </div>
  );
};

export default AddCar;