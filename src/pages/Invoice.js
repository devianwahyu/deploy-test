import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Worker, Viewer } from '@react-pdf-viewer/core';
import '@react-pdf-viewer/core/lib/styles/index.css';
import { BiArrowBack } from 'react-icons/bi';
import { BsFillCheckCircleFill } from 'react-icons/bs';
import { AiOutlineMinus } from 'react-icons/ai';
import { FiDownload } from 'react-icons/fi';
import fileUrl from '../assets/invoice_test.pdf';

import Navbar from '../components/Navbar';
import Footer from '../components/Footer';

export const PdfViewer = () => {
  return (
    <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js">
      <div className='w-full'>
        <Viewer fileUrl={fileUrl} />
      </div>
    </Worker>
  );
};

const Invoice = () => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/detail');
  };

  return (
    <div className='flex flex-col justify-between h-screen'>
      <div className='bg-secondary h-40 flex flex-col justify-between'>
        <Navbar />
        <div className='px-56 mb-3'>
          <div className='flex flex-wor justify-between'>
            <div className='flex flex-row'>
              <BiArrowBack size={25} onClick={handleClick} />
              <div className='flex flex-col ml-3'>
                <p className='font-semibold text-lg'>Tiket</p>
                <p>Order ID: xxxxxxx</p>
              </div>
            </div>
            <div className='flex flex-row gap-2'>
              <div className='flex flex-row items-center'>
                <BsFillCheckCircleFill color='#0D28A6' />
                <span className='mx-1 text-gray-600'>Pilih Metode</span>
                <AiOutlineMinus size={40} color='#0D28A6' />
              </div>
              <div className='flex flex-row items-center'>
                <BsFillCheckCircleFill color='#0D28A6' />
                <span className='mx-1 text-gray-600'>Bayar</span>
                <AiOutlineMinus size={40} color='#0D28A6' />
              </div>
              <div className='flex flex-row items-center'>
                <BsFillCheckCircleFill color='#0D28A6' />
                <span className='ml-1 text-gray-600'>Tiket</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='flex flex-col items-center mt-16'>
        <BsFillCheckCircleFill color='#5CB85F' size={60} />
        <h3 className='font-bold text-2xl mt-4'>Pembayaran Berhasil!</h3>
        <p className='text-gray-600 mt-3'>Tunjukkan invoice ini ke petugas BCR di titik temu.</p>
        <div className='bg-white p-7 shadow-sm rounded w-[35vw] mt-4 mb-8'>
          <div className='flex flex-row justify-between mb-5'>
            <div>
              <p className='text-lg font-semibold'>Invoice</p>
              <p>Car Invoice</p>
            </div>
            <a href={fileUrl} download={true} className='flex flex-row justify-center items-center h-9 border-2 border-blue-700 text-blue-700 hover:bg-blue-700 hover:text-white rounded px-2'>
              <FiDownload />
              <p>Unduh</p>
            </a>
          </div>
          <PdfViewer />
        </div>
      </div>
      <div className='px-40 mt-4'>
        <Footer />
      </div>
    </div>
  );
};

export default Invoice;