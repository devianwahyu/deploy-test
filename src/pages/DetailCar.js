import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import Navbar from '../components/Navbar';
import Filter from '../components/detail-car/Filter';
import About from '../components/detail-car/About';
import Card from '../components/detail-car/Card';
import Footer from '../components/Footer';
import Loader from '../components/Loader';

const DetailCar = () => {
  const navigate = useNavigate();

  const [car, setCar] = useState({});
  const [loading, setLoading] = useState(false);

  const getCar = async () => {
    try {
      setLoading(true);
      let dataFromAPI = await fetch(`https://rent-cars-api.herokuapp.com/admin/car/90`);
      let dataToJSON = await dataFromAPI.json();
      setCar(await dataToJSON);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCar();
  }, []);

  const handleClick = () => {
    navigate('/invoice');
  };

  if (loading) return <Loader />;

  return (
    <div>
      <div className='bg-secondary h-80'>
        <Navbar />
      </div>
      <div className='absolute top-[250px] left-[160px] right-[160px]'>
        <Filter />
      </div>
      <div className='absolute top-[450px] left-[160px] right-[160px]'>
        <div className='flex flex-row gap-9'>
          <div className='flex flex-col items-end'>
            <About />
            <button className='bg-green p-2 mt-6 text-white font-semibold rounded' onClick={handleClick}>Lanjutkan Pembayaran</button>
          </div>
          <div>
            <Card key={car.id} id={car.id} image={car.image} name={car.name} price={car.price} />
          </div>
        </div>
        <div className='my-14'>
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default DetailCar;