import React from 'react';
import { BsPeople, BsCalendar4Event } from 'react-icons/bs';
import { AiOutlineSetting } from 'react-icons/ai';
import ModalImage from 'react-modal-image';
import { useNavigate } from 'react-router-dom';

const Card = ({ id, image, name, price }) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/invoice');
  };

  return (
    <div className='flex flex-col bg-white shadow-md p-3'>
      <ModalImage
        small={image ? image : '/assets/img_car.png'}
        large={image ? image : '/assets/img_car.png'}
        alt={name}
      />
      <h3 className='text-lg font-semibold mt-3'>{name ? name : 'Batman Car'}/Mobil</h3>
      <div className='flex flex-row gap-4 text-gray-600'>
        <div className='flex flex-row items-center gap-1'>
          <BsPeople />
          <p>4 orang</p>
        </div>
        <div className='flex flex-row items-center gap-1'>
          <AiOutlineSetting />
          <p>Manual</p>
        </div>
        <div className='flex flex-row items-center gap-1'>
          <BsCalendar4Event />
          <p>Tahun 2022</p>
        </div>
      </div>
      <div className='mt-10 flex flex-row justify-between'>
        <p>Total</p>
        <p className='font-semibold'>Rp {price ? Number(price).toLocaleString("id-ID") : 'Free'}</p>
      </div>
      <button className='bg-green p-2 font-semibold text-white mt-3 rounded' onClick={handleClick}>Lanjutkan Pembayaran</button>
    </div>
  );
};

export default Card;