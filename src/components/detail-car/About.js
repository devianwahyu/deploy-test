import React from 'react';
import { BsDot } from 'react-icons/bs';

const About = () => {
  const includes = [
    'Apa saja yang termasuk dalam paket misal durasi max 12 jam',
    'Sudah termasuk bensin selama 12 jam',
    'Sudah termasuk Tiket Wisata',
    'Sudah termasuk pajak'
  ];

  const excludes = [
    'Tidak termasuk biaya makan sopir Rp 75.000/hari',
    'Jika overtime lebih dari 12 jam akan ada biaya tambahan Rp 20.000/jam',
    'Tidak termasuk akomodasi penginapan'
  ];

  const rules = [
    'Tidak termasuk biaya makan sopir Rp 75.000/hari',
    'Jika overtime lebih dari 12 jam akan ada biaya tambahan Rp 20.000/jam',
    'Tidak termasuk akomodasi penginapan',
    'Tidak termasuk biaya makan sopir Rp 75.000/hari',
    'Jika overtime lebih dari 12 jam akan ada biaya tambahan Rp 20.000/jam',
    'Tidak termasuk akomodasi penginapan',
    'Tidak termasuk biaya makan sopir Rp 75.000/hari',
    'Jika overtime lebih dari 12 jam akan ada biaya tambahan Rp 20.000/jam',
    'Tidak termasuk akomodasi penginapan'
  ];

  const listGenerator = (list) => {
    const components = [];

    for (let i in list) {
      components.push(
        <li key={i} className='flex flex-row items-center text-gray-600'>
          <BsDot size={25} />
          <p>{list[i]}</p>
        </li>
      );
    }

    return components;
  };

  return (
    <div className='flex flex-col bg-white shadow-md p-5 w-[50vw] rounded'>
      <div>
        <h3 className='text-lg font-semibold'>Tentang Paket</h3>
        <h5 className='mt-2'>Include</h5>
        <ul>
          {listGenerator(includes)}
        </ul>
        <h5 className='mt-2'>Exclude</h5>
        <ul>
          {listGenerator(excludes)}
        </ul>
      </div>
      <div className='mt-3'>
        <h3 className='text-lg font-semibold'>Refund, Reschedule, Overtime</h3>
        <ul>
          {listGenerator(rules)}
        </ul>
      </div>
    </div >
  );
};

export default About;