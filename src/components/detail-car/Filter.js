import React from 'react';

const Filter = () => {
  return (
    <div className='bg-white shadow-md px-5 py-6 rounded'>
      <h3 className='font-semibold text-lg'>Pencarianmu</h3>
      <div className='flex flex-row gap-3 justify-between mt-3'>
        <div className='flex flex-col flex-grow'>
          <label className='text-gray-600'>Tipe Driver</label>
          <input className='bg-gray-200 mt-1 rounded border-2 border-gray-300 p-1' type={'text'} disabled={true} />
        </div>
        <div className='flex flex-col flex-grow'>
          <label className='text-gray-600'>Tanggal</label>
          <input className='bg-gray-200 mt-1 rounded border-2 border-gray-300 p-1' type={'text'} disabled={true} />
        </div>
        <div className='flex flex-col flex-grow'>
          <label className='text-gray-600'>Waktu Jemput/Ambil</label>
          <input className='bg-gray-200 mt-1 rounded border-2 border-gray-300 p-1' type={'text'} disabled={true} />
        </div>
        <div className='flex flex-col flex-grow'>
          <label className='text-gray-600'>Jumlah Penumpang(optional)</label>
          <input className='bg-gray-200 mt-1 rounded border-2 border-gray-300 p-1' type={'text'} disabled={true} />
        </div>
      </div>
    </div>
  );
};

export default Filter;