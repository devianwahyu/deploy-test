import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Landing from './pages/Landing';
import Dashboard from './pages/Dashboard';
import DetailCar from './pages/DetailCar';
import Invoice from './pages/Invoice';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Landing />} />
        <Route path='/dashboard' element={<Dashboard />} />
        <Route path='/detail' element={<DetailCar />} />
        <Route path='/invoice' element={<Invoice />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;